//
// Created by pc on 17/12/2020.
//

#include "SceneWidget.h"
#include <QGLWidget>
#include <GL/glu.h>
#include <GL/glut.h>
#include <cmath>
#include "Building.h"
#include "Table.h"
#include <QDebug>




//Constructor
SceneWidget::SceneWidget(QWidget *parent):QGLWidget(parent),
_image0("seamless.ppm"),
_image1("earth.ppm"),
_image2("Marc_Dekamps.ppm"),
_image3("banksy.ppm"),
_camerax(1.0),
_cameray(1.0),
_cameraz(1.0),
_changex(40.0),
_changey(40.0),
_changez(40.0),
_angle(0.0),
_increment(1.0),
currentMaterial(&whiteShinyMaterials)
{
    //Assign pointer to building in order to swap building objects
    frame_coords = (GLfloat **)malloc(sizeof(full_house)*sizeof(full_house[0]));
    building_size = sizeof(full_house)/sizeof(full_house[0]);
    block_size = 3;
    for(int i=0;i<sizeof(full_house)/sizeof(full_house[0]);i++){
        frame_coords[i] = (GLfloat *)malloc(sizeof(full_house[0]));
        for(int j=0;j<block_size;j++){
            frame_coords[i][j] = full_house[i][j];
        }
    }
}

//Called when OpenGL is setup
void SceneWidget::initializeGL()
{
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_NORMALIZE);
    //glEnable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);
    GLfloat light_pos[] = {0.,0.,1.,0.};
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glLightfv(GL_LIGHT0,GL_POSITION,light_pos);


    //Generate two texture objects
    glGenTextures(2,_texture_objects);

    //Setup brick Texture
    glBindTexture(GL_TEXTURE_2D, _texture_objects[0]);
    glTexImage2D(GL_TEXTURE_2D,0,GL_RGB,_image0.Width(),_image0.Height(),0,GL_RGB,GL_UNSIGNED_BYTE,_image0.imageField());
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

   //Setup map Texture
    glBindTexture(GL_TEXTURE_2D, _texture_objects[1]);
    glTexImage2D(GL_TEXTURE_2D,0,GL_RGB,_image1.Width(),_image1.Height(),0,GL_RGB,GL_UNSIGNED_BYTE,_image1.imageField());
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    //Setup banksy texture
    glBindTexture(GL_TEXTURE_2D, _texture_objects[2]);
    glTexImage2D(GL_TEXTURE_2D,0,GL_RGB,_image2.Width(),_image2.Height(),0,GL_RGB,GL_UNSIGNED_BYTE,_image2.imageField());
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glBindTexture(GL_TEXTURE_2D, _texture_objects[3]);
    glTexImage2D(GL_TEXTURE_2D,0,GL_RGB,_image3.Width(),_image3.Height(),0,GL_RGB,GL_UNSIGNED_BYTE,_image3.imageField());
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);



    //Setup sphere quadric
    sphere = gluNewQuadric();


    //Set background colour
    //glClearColor(1.0,0.0,0.0,0.0);
}
//Called when the widget is resized
void SceneWidget::resizeGL(int w, int h)
{
    //Set the viewport
    glViewport(0,0,w,h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-_changex,_changex,-_changey,_changey,-_changez,_changez);
    glMatrixMode(GL_MODELVIEW);
}

void SceneWidget::updateMaterial(struct materialStruct *material){
    currentMaterial = material;
}

//Building
void SceneWidget::building(){
    //Coordinates to draw the frame of building 3 coordinates for each cube
    //Use brick texture
    glBindTexture(GL_TEXTURE_2D,_texture_objects[0]);
    for(int i=0;i<(building_size);i++){
        glTranslatef(frame_coords[i][0],frame_coords[i][1],frame_coords[i][2]);
        this->cube(&whiteShinyMaterials);

        glTranslatef(-frame_coords[i][0],-frame_coords[i][1],-frame_coords[i][2]);
    }
    //Draw a window on wall with blue material
    //Disable texture for now
    glBindTexture(GL_TEXTURE_2D, 0);
    for(int i=0;i<((sizeof(window_coords)/sizeof(window_coords[0])));i++){
        glTranslatef(window_coords[i][0],window_coords[i][1],window_coords[i][2]);
        this->cube(&windowMaterials);
        glTranslatef(-window_coords[i][0],-window_coords[i][1],-window_coords[i][2]);
    }
    glBindTexture(GL_TEXTURE_2D, _texture_objects[2]);
    for(int i=0;i<((sizeof(roof_coords)/sizeof(roof_coords[0])));i++){
        glTranslatef(roof_coords[i][0],roof_coords[i][1],roof_coords[i][2]);
        this->cube(&whiteShinyMaterials);
        glTranslatef(-roof_coords[i][0],-roof_coords[i][1],-roof_coords[i][2]);
    }
    glTranslatef(2,0,2);
    this->table();
    glTranslatef(-2,0,-2);
}

//Function to create a garage
void SceneWidget::garage(materialStruct *p_front){
    int iterations = 5;
    int change_x=2;
    int change_y=2;
    int change_z=-2;
    //Draw front and back of garage
    for(int i=0;i<2;i++) {
        glPushMatrix();
        glTranslatef(0,0,-10*i);
        for (int i = 0; i < iterations; i++) {
            glTranslatef(change_x, change_y, 0);
            this->cube(p_front);
        }
        for (int i = 0; i < iterations - 1; i++) {
            glTranslatef(change_x, -change_y, 0);
            this->cube(p_front);
        }
        glPopMatrix();
    }
    glTranslatef(iterations*change_x,iterations*change_y,0);
    for(int i=0;i<iterations;i++){
        glTranslatef(0,0,change_z);
        this->cube(p_front);
    }
}

//Draw the floor of the scene
void SceneWidget::floor(){
    //Move to start of grid
    glBindTexture(GL_TEXTURE_2D, 0);
    glTranslatef(-_changex,0,-_changez);
    for(int j=0;j<=_changex*2;j++) {
        for (int i =0; i <= _changez*2; i++) {
            this->cube(currentMaterial);
            glTranslatef(0, 0, 1);
        }
        glTranslatef(1,0,0);
        glTranslatef(0,0,-_changez*2-1);
    }
}


void SceneWidget::table(){
    glPushMatrix();
    glScalef(0.5,0.5,0.5);
    for(int i=0;i<((sizeof(leg_coords)/sizeof(leg_coords[0])));i++){
        glTranslatef(leg_coords[i][0],leg_coords[i][1],leg_coords[i][2]);
        this->cylinder();
        glTranslatef(-leg_coords[i][0],-leg_coords[i][1],-leg_coords[i][2]);
    }
    glBindTexture(GL_TEXTURE_2D, _texture_objects[3]);
    glBegin(GL_POLYGON);
    glNormal3f(top_normal[0],top_normal[1],top_normal[2]);
    for(int i=0;i<(sizeof(top_coords)/sizeof(top_coords[0]));i++){
        glTexCoord2f(tex_coords[i][0],tex_coords[i][1]);
        glVertex3f(top_coords[i][0],top_coords[i][1],top_coords[i][2]);
    }
    glEnd();
    glBindTexture(GL_TEXTURE_2D,0);
    glTranslatef(leg_coords[0][1],4.5,0);
    glutSolidTeapot(1.0);
    glPopMatrix();
    //Place solid tea pot on table
}

//Create weird shapes rotating cubes
void SceneWidget::sphereCube(){
    float radius = 5.0;
    float steps = 720;
    float x,z,angle;
    glPushMatrix();
    for(int i=1;i<6;i++) {
        for (int i = 0; i < steps; i++) {
            angle = i;
            glTranslatef(radius * cos(angle), 0, radius * sin(angle));
            glutSolidCube(5.0);
            glTranslatef(-radius * cos(angle), 0, -radius * sin(angle));
        }
        glTranslatef(0,1,0);
    }
    glPopMatrix();
};


//Function to draw the cube
void SceneWidget::cube(materialStruct *p_front){

    // Here are the normals, correctly calculated for the cube faces below
    GLfloat normals[][3] = { {1., 0. ,0.}, {-1., 0., 0.}, {0., 0., 1.}, {0., 0., -1.}, {0, 1, 0}, {0, -1, 0} };

    // create one face with the material passed in
    glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
    glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);


    glNormal3fv(normals[0]);
    glBegin(GL_POLYGON);
    glTexCoord2f(0.,0.);
    glVertex3f( 1.0, -1.0,  1.0);
    glTexCoord2f(1.,0.);
    glVertex3f( 1.0, -1.0, -1.0);
    glTexCoord2f(1.,1.);
    glVertex3f( 1.0,  1.0, -1.0);
    glTexCoord2f(0.,1.);
    glVertex3f( 1.0,  1.0,  1.0);
    glEnd();


    glNormal3fv(normals[3]);
    glBegin(GL_POLYGON);
    glTexCoord2f(0.,0.);
    glVertex3f(-1.0, -1.0, -1.0);
    glTexCoord2f(1.,0.);
    glVertex3f( 1.0, -1.0, -1.0);
    glTexCoord2f(1.,1.);
    glVertex3f( 1.0,  1.0, -1.0);
    glTexCoord2f(0.,1.);
    glVertex3f(-1.0,  1.0, -1.0);
    glEnd();


    glNormal3fv(normals[2]);
    glBegin(GL_POLYGON);
    glTexCoord2f(0.,0.);
    glVertex3f(-1.0, -1.0, 1.0);
    glTexCoord2f(1.0,0.);
    glVertex3f( 1.0, -1.0, 1.0);
    glTexCoord2f(1.,1.);
    glVertex3f( 1.0,  1.0, 1.0);
    glTexCoord2f(0.,1.);
    glVertex3f(-1.0,  1.0, 1.0);
    glEnd();


    glNormal3fv(normals[1]);
    glBegin(GL_POLYGON);
    glTexCoord2f(0.,0.);
    glVertex3f( -1.0, -1.0,  1.0);
    glTexCoord2f(1.,0.);
    glVertex3f( -1.0, -1.0, -1.0);
    glTexCoord2f(1.,1.);
    glVertex3f( -1.0,  1.0, -1.0);
    glTexCoord2f(0.,1.);
    glVertex3f( -1.0,  1.0,  1.0);
    glEnd();


    glNormal3fv(normals[4]);
    glBegin(GL_POLYGON);
    glVertex3f(  1.0,  1.0,  1.0);
    glVertex3f(  1.0,  1.0, -1.0);
    glVertex3f( -1.0,  1.0, -1.0);
    glVertex3f( -1.0,  1.0,  1.0);
    glEnd();


    glNormal3fv(normals[5]);
    glBegin(GL_POLYGON);
    glVertex3f(  1.0,  -1.0,  1.0);
    glVertex3f(  1.0,  -1.0, -1.0);
    glVertex3f( -1.0,  -1.0, -1.0);
    glVertex3f( -1.0,  -1.0,  1.0);
    glEnd();



}
void SceneWidget::cylinder(){
    glMatrixMode(GL_MODELVIEW);

    float x0, x1, y0, y1;

    float z_min = -0.2;
    float z_max =  0.2;

    int N=100;
    int n_div=1;
    float delta_z = (z_max - z_min)/n_div;

    for (int i = 0; i < N; i++){
        for(int i_z = 0; i_z < n_div; i_z++){
            x0 = cos(2*i*M_PI/N);
            x1 = cos(2*(i+1)*M_PI/N);
            y0 = sin(2*i*M_PI/N);
            y1 = sin(2*(i+1)*M_PI/N);

            float z = z_min + i_z*delta_z;
            glBegin(GL_POLYGON);
            glVertex3f(x0,z,y0);
            glNormal3f(x0,0,y0);
            glVertex3f(x1,z,y1);
            glNormal3f(x1,0,y1);
            glVertex3f(x1,z+delta_z,y1);
            glNormal3f(x1,0,y1);
            glVertex3f(x0,z+delta_z,y0);
            glNormal3f(x0,0,y0);
            glEnd();
        }
    }
}
void SceneWidget::tetrahedron() {
//    glBegin(GL_TRIANGLES);
//    glEnd(GL_TRIANGLES);
}

//Texture mapped onto a quadric using gl
void SceneWidget::world(){
    //Set shiny material
    glMaterialfv(GL_FRONT, GL_AMBIENT,    whiteShinyMaterials.ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE,    whiteShinyMaterials.diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR,   whiteShinyMaterials.specular);
    glMaterialf(GL_FRONT, GL_SHININESS,   whiteShinyMaterials.shininess);

    gluQuadricDrawStyle(sphere, GLU_FILL);
    //Calculate tex coords
    gluQuadricTexture(sphere, GL_TRUE);
    //Calculate normals to points
    gluQuadricNormals(sphere, GLU_SMOOTH);
    //Draw sphere
    gluSphere(sphere,1.0,_sphereSlices,_sphereStacks);
}

//Slots for debugging the camera and world
void SceneWidget::updateAngle() {
    _angle += _increment;
    this->repaint();
}
void SceneWidget::changeAngle(int angle){
    _increment = angle;
}
void SceneWidget::updateX(int x){
    _changex = (float)x + 10.0;
    this->repaint();
}
void SceneWidget::updateY(int y){
    _changey = (float)y + 10.0;
    this->repaint();
}
void SceneWidget::updateZ(int z){
    _changez = (float)z + 10.0;

    this->repaint();
}

void SceneWidget::cameraX(int x) {
    _camerax = (float)x/10.0;
}
void SceneWidget::cameraY(int y) {
    _cameray = (float)y/10.0;
}
void SceneWidget::cameraZ(int z) {
    _cameraz = (float)z/10.0;
}
void SceneWidget::toggleWall() {
    bool toggle;
    qDebug() << "INSIDE";
    if(building_size==sizeof(full_house)/sizeof(full_house[0])){
        building_size = sizeof(open_house)/sizeof(open_house[0]);
        toggle=0;
    }
    else{
        building_size=sizeof(full_house)/sizeof(full_house[0]);
        toggle=1;
    }
        for (int i = 0; i < sizeof(full_house) / sizeof(full_house[0]); i++) {
            frame_coords[i] = (GLfloat *) malloc(sizeof(full_house[0]));
            for (int j = 0; j < block_size; j++) {
                if(toggle) {
                    frame_coords[i][j] = full_house[i][j];
                }
                else {
                    frame_coords[i][j] = open_house[i][j];
                }
            }
        }
    }



//Slot to repaint the world Not needed atm
void SceneWidget::paint(){
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-_changex-10,_changex+10,-_changey-10,_changey+10,-_changez-10,_changez+10);
    glMatrixMode(GL_MODELVIEW);
}

//Called everytime the widget is updated
void SceneWidget::paintGL(){
    glEnable(GL_DEPTH_TEST);
    //Clear the widget
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    //Setup the camera
    gluLookAt(_camerax,_cameray,_cameraz,0.,0.,0.,0.,1.,0.);


    GLfloat light_pos[] = {1.,1.,1.,0.};

    //Setup lighting
    glPushMatrix();
    glLoadIdentity();
    glLightfv(GL_LIGHT0,GL_POSITION,light_pos);
    glPopMatrix();
    glPushMatrix();



    glTranslatef(-_changex,0,-_changez);

    //Draw globe on top of roof
    glPushMatrix();
    glTranslatef(5,20,5);
    //Bind to globe texture
    glBindTexture(GL_TEXTURE_2D, _texture_objects[1]);
    glRotatef(_angle,1,0,0);
    glScalef(3,3,3);
    this->world();
    //Bind to brick texture
    glBindTexture(GL_TEXTURE_2D, _texture_objects[0]);
    glPopMatrix();

    //Draw building
    this->building();

    //Move to next house
    glTranslatef(0,0,15);

    glPushMatrix();
    glTranslatef(5,20,5);
    glBindTexture(GL_TEXTURE_2D, _texture_objects[1]);
    glRotatef(_angle,1,0,0);
    glScalef(3,3,3);
    this->world();
    glBindTexture(GL_TEXTURE_2D, _texture_objects[0]);
    glPopMatrix();

    this->building();

    glTranslatef(0,0,15);

    glPushMatrix();
    glTranslatef(5,20,5);
    glBindTexture(GL_TEXTURE_2D, _texture_objects[1]);
    glRotatef(_angle,1,0,0);
    glScalef(3,3,3);
    this->world();
    glBindTexture(GL_TEXTURE_2D, _texture_objects[0]);
    glPopMatrix();

    this->building();

    glTranslatef(0,0,15);

    glPushMatrix();
    glTranslatef(5,20,5);
    glBindTexture(GL_TEXTURE_2D, _texture_objects[1]);
    glRotatef(_angle,1,0,0);
    glScalef(3,3,3);
    this->world();
    glBindTexture(GL_TEXTURE_2D, _texture_objects[0]);
    glPopMatrix();

    this->building();

    glPopMatrix();

    //Draw the table
    glPushMatrix();
    glTranslatef(_changex/3,0,0);
    glScalef(2.,2.,2.);
    this->building();
    glTranslatef(-_changex/3,0,5);
    glScalef(0.5,0.5,0.5);
    glPopMatrix();

    //draw floor
    glPushMatrix();
    //Move floor down 1
    glTranslatef(0,-2,0);
    //Use no texture for the floor
    glBindTexture(GL_TEXTURE_2D,0);
    this->floor();
    glPopMatrix();


    glFlush();
}

