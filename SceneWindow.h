//
// Created by pc on 14/12/2020.
//

#ifndef TEST_SCENEWINDOW_H
#define TEST_SCENEWINDOW_H
#include <QSlider>
#include <QWidget>
#include <QMainWindow>
#include <QMenu>
#include <QMenuBar>
#include <QAction>
#include <QBoxLayout>
#include <QLabel>
#include <QMainWindow>
#include <QTimer>
#include <QCheckBox>
#include <QtWidgets/QLineEdit>
#include "UnitsLabel.h"
#include "SceneWidget.h"


class SceneWindow:public QWidget {
    Q_OBJECT
    public:
        SceneWindow(QWidget *parent);

    protected:
    void closeEvent(QCloseEvent *event);
    void passMaterial();
    QMenuBar *menuBar;
    // file menu
    QMenu *fileMenu;
    //file action list
    QList<QAction *> fileActions;
    // quit action
    QAction *actionQuit;
    QAction *actionDialog;
    QAction *actionDebugging;
    QAction *actionRotation;

    // window layout
    QBoxLayout *windowLayout;
    QGridLayout *controlsLayout;

    //Dialogs
    QDialog *materialsDialog;
    QDialog *worldDialog;
    QDialog *rotationDialog;

    //Rotation speed
    QSlider *rotationSlider;
    UnitsLabel *rotationSpeed;

    //World Grid
    QSlider *worldSize;
    QSlider *x;
    QSlider *y;
    QSlider *z;

    //World Dialog menus
    QLabel *worldProperties;
    QLabel *cameraProperties;

    //Camera Position
    QSlider *cameraX;
    QSlider *cameraY;
    QSlider *cameraZ;


    //World Grid labels
    UnitsLabel *xLabel;
    UnitsLabel *yLabel;
    UnitsLabel *zLabel;
    UnitsLabel *worldLabel;

    //Camera Postion labels
    UnitsLabel *cameraXLabel;
    UnitsLabel *cameraYLabel;
    UnitsLabel *cameraZLabel;


    //GLWIDGET
    SceneWidget *sceneWidget;
    // a timer
    QTimer *ptimer;

    //Open house
    QCheckBox *house;

    //Material Properties dialog box
    QLabel *ambientLabel;
    QLabel *diffuseLabel;
    QLabel *specularLabel;
    QLabel *shineLabel;
    //Material properties
    materialStruct *material;
    QLineEdit *ambient;
    QLineEdit *ambient1;
    QLineEdit *ambient2;
    QLineEdit *ambient3;
    QLineEdit *diffuse;
    QLineEdit *diffuse1;
    QLineEdit *diffuse2;
    QLineEdit *diffuse3;
    QLineEdit *specular;
    QLineEdit *specular1;
    QLineEdit *specular2;
    QLineEdit *specular3;
    QLineEdit *shine;


};


#endif //TEST_SCENEWINDOW_H
