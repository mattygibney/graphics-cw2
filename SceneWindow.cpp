//
// Created by pc on 14/12/2020.
//

#include "SceneWindow.h"
#include "SceneWidget.h"
#include <QDebug>
#include <iostream>
#include <QtWidgets/QDialog>
#include <QtWidgets/QInputDialog>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QPushButton>


SceneWindow::SceneWindow(QWidget *parent): QWidget(parent) {

    //Setup menu bar
    menuBar = new QMenuBar(this);
    //Setup file menu
    fileMenu = new QMenu("File");
    //Create file actions
    actionQuit = new QAction(QString("Quit"),parent);
    actionDialog = new QAction(QString("Material Properties"),parent);
    actionDebugging = new QAction(QString("Camera and World Properties"),parent);
    actionRotation = new QAction(QString("Rotation Speed"),parent);


    //Create file action list
    fileActions.append(actionQuit);
    fileActions.append(actionDialog);
    fileActions.append(actionDebugging);
    fileActions.append(actionRotation);

    //Add actions to file menu
    fileMenu->addActions(fileActions);
    //Add fileMenu to menuBar
    menuBar->addMenu(fileMenu);

    sceneWidget = new SceneWidget(this);

    //Create a new window layout
    windowLayout = new QBoxLayout(QBoxLayout::TopToBottom,this);
    //Put all controls into a layout
    controlsLayout = new QGridLayout();
    house = new QCheckBox("Open Houses");
    windowLayout->addWidget(house);
    windowLayout->addWidget(sceneWidget);

    //Contents margin to stop covering menu
    windowLayout->setContentsMargins(10,menuBar->height()+10,10,10);



    worldDialog = new QDialog();
    worldDialog->setFixedSize(512,512);
    worldDialog->setLayout(controlsLayout);

    //Dialog Labels
    worldProperties = new QLabel("World Sizes");
    cameraProperties = new QLabel("Camera Properties");

    //World settings
    int minWorld = 30;
    worldSize = new QSlider(Qt::Horizontal);
    worldSize->setMinimum(minWorld);
    x = new QSlider(Qt::Horizontal);
    x->setMinimum(minWorld);
    y = new QSlider(Qt::Horizontal);
    y->setMinimum(minWorld);
    z = new QSlider(Qt::Horizontal);
    z->setMinimum(minWorld);
    xLabel = new UnitsLabel("X");
    yLabel = new UnitsLabel("Y");
    zLabel = new UnitsLabel("Z");
    worldLabel = new UnitsLabel("");

    //Camera settings
    cameraX = new QSlider(Qt::Horizontal);
    cameraY = new QSlider(Qt::Horizontal);
    cameraZ = new QSlider(Qt::Horizontal);
    cameraXLabel = new UnitsLabel("CAM X");
    cameraYLabel = new UnitsLabel("Cam Y");
    cameraZLabel = new UnitsLabel("Cam Z");
    cameraX->setMinimum(-50);
    cameraX->setMaximum(50);
    cameraY->setMinimum(-50);
    cameraY->setMaximum(50);
    cameraZ->setMinimum(-50);
    cameraZ->setMaximum(50);

    //Connect world sliders to slots
    connect(x,SIGNAL(valueChanged(int)),sceneWidget,SLOT(updateX(int)));
    connect(y,SIGNAL(valueChanged(int)),sceneWidget,SLOT(updateY(int)));
    connect(z,SIGNAL(valueChanged(int)),sceneWidget,SLOT(updateZ(int)));

    connect(x,SIGNAL(valueChanged(int)),xLabel,SLOT(updateValue(int)));
    connect(y,SIGNAL(valueChanged(int)),yLabel,SLOT(updateValue(int)));
    connect(z,SIGNAL(valueChanged(int)),zLabel,SLOT(updateValue(int)));

    connect(worldSize,SIGNAL(valueChanged(int)),x,SLOT(setValue(int)));
    connect(worldSize,SIGNAL(valueChanged(int)),y,SLOT(setValue(int)));
    connect(worldSize,SIGNAL(valueChanged(int)),z,SLOT(setValue(int)));

    connect(worldSize,SIGNAL(valueChanged(int)),worldLabel,SLOT(updateValue(int)));


    //Connect camera sliders to slots
    connect(cameraX,SIGNAL(valueChanged(int)),sceneWidget,SLOT(cameraX(int)));
    connect(cameraY,SIGNAL(valueChanged(int)),sceneWidget,SLOT(cameraY(int)));
    connect(cameraZ,SIGNAL(valueChanged(int)),sceneWidget,SLOT(cameraZ(int)));

    connect(cameraX,SIGNAL(valueChanged(int)),cameraXLabel,SLOT(updateValue(int)));
    connect(cameraY,SIGNAL(valueChanged(int)),cameraYLabel,SLOT(updateValue(int)));
    connect(cameraZ,SIGNAL(valueChanged(int)),cameraZLabel,SLOT(updateValue(int)));

    //Set camera eye point at (1,1,1)
    cameraX->setValue(1);
    cameraY->setValue(1);
    cameraZ->setValue(1);

    //Start world with initial size 40x40x40
    x->setValue(40);
    y->setValue(40);
    z->setValue(40);
    worldSize->setValue(40);



    //World add widgets
    controlsLayout->addWidget(worldProperties,0,0);
    controlsLayout->addWidget(worldSize,0,1);
    controlsLayout->addWidget(worldLabel,0,2);
    controlsLayout->addWidget(x,1,0);
    controlsLayout->addWidget(xLabel,1,1);
    controlsLayout->addWidget(y,2,0);
    controlsLayout->addWidget(yLabel,2,1);
    controlsLayout->addWidget(z,3,0);
    controlsLayout->addWidget(zLabel,3,1);

    controlsLayout->addWidget(cameraProperties,4,0);
    controlsLayout->addWidget(cameraX,5,0);
    controlsLayout->addWidget(cameraXLabel,5,1);
    controlsLayout->addWidget(cameraY,6,0);
    controlsLayout->addWidget(cameraYLabel,6,1);
    controlsLayout->addWidget(cameraZ,7,0);
    controlsLayout->addWidget(cameraZLabel,7,1);

    //Setup dialog box to edit material properties
    materialsDialog = new QDialog();
    materialsDialog->setWindowTitle("Floor Material Properties");
    QGridLayout *dialogLayout = new QGridLayout(materialsDialog);
    dialogLayout->setVerticalSpacing(0);
    ambientLabel = new QLabel("Ambient");
    diffuseLabel = new QLabel("Diffuse");
    specularLabel = new QLabel("Specular");
    shineLabel = new QLabel("Shine");
    ambient = new QLineEdit("1.0");
    ambient1 = new QLineEdit("1.0");
    ambient2 = new QLineEdit("1.0");
    ambient3 = new QLineEdit("1.0");
    diffuse = new QLineEdit("1.0");
    diffuse1 = new QLineEdit("1.0");
    diffuse2 = new QLineEdit("1.0");
    diffuse3 = new QLineEdit("1.0");
    specular = new QLineEdit("1.0");
    specular1 = new QLineEdit("1.0");
    specular2 = new QLineEdit("1.0");
    specular3 = new QLineEdit("1.0");
    shine = new QLineEdit("0.0");

    QPushButton *button = new QPushButton("Apply");
    QPushButton *exit = new QPushButton("Exit");

    dialogLayout->addWidget(ambientLabel,0,0);
    dialogLayout->addWidget(ambient,1,0);
    dialogLayout->addWidget(ambient1,1,1);
    dialogLayout->addWidget(ambient2,1,2);
    dialogLayout->addWidget(ambient3,1,3);
    dialogLayout->addWidget(diffuseLabel,2,0);
    dialogLayout->addWidget(diffuse,3,0);
    dialogLayout->addWidget(diffuse1,3,1);
    dialogLayout->addWidget(diffuse2,3,2);
    dialogLayout->addWidget(diffuse3,3,3);
    dialogLayout->addWidget(specularLabel,4,0);
    dialogLayout->addWidget(specular,5,0);
    dialogLayout->addWidget(specular1,5,1);
    dialogLayout->addWidget(specular2,5,2);
    dialogLayout->addWidget(specular3,5,3);
    dialogLayout->addWidget(shineLabel,6,0);
    dialogLayout->addWidget(shine,7,0);
    dialogLayout->addWidget(button,8,0);

    //Create new material
    material = new materialStruct();


    materialsDialog->setLayout(dialogLayout);
    materialsDialog->setFixedHeight(512);
    materialsDialog->setFixedWidth(512);

    //Rotation Dialog
    rotationDialog = new QDialog();
    rotationSlider = new QSlider(Qt::Horizontal);
    rotationSpeed = new UnitsLabel("Degrees per 10 frames");
    QHBoxLayout *rotationLayout = new QHBoxLayout(rotationDialog);
    rotationLayout->addWidget(rotationSlider);
    rotationLayout->addWidget(rotationSpeed);
    rotationDialog->setLayout(rotationLayout);
    connect(rotationSlider, SIGNAL(valueChanged(int)), rotationSpeed, SLOT(updateValue(int)));
    rotationSlider->setValue(10);
    connect(rotationSlider, SIGNAL(valueChanged(int)), sceneWidget, SLOT(changeAngle(int)));

    //Actions
    connect(actionQuit, SIGNAL(triggered()),this,SLOT(close()));
    connect(actionQuit, SIGNAL(triggered()),materialsDialog,SLOT(close()));
    connect(actionQuit, SIGNAL(triggered()),worldDialog,SLOT(close()));
    connect(actionDialog, SIGNAL(triggered()),materialsDialog,SLOT(open()));
    connect(actionDebugging, SIGNAL(triggered()),worldDialog,SLOT(open()));
    connect(actionRotation, SIGNAL(triggered()),rotationDialog, SLOT(open()));

    connect(house, &QCheckBox::toggled,sceneWidget,&SceneWidget::toggleWall);

    ptimer = new QTimer(this);
    ptimer->start(10);
    //Make sure the widget constantly updates the camera and grid
    connect(ptimer,SIGNAL(timeout()), sceneWidget, SLOT(paint()));
    //Rotate the globe
    connect(ptimer, SIGNAL(timeout()), sceneWidget, SLOT(updateAngle()));

    //Update the material when the done button is pressed
    connect(button,&QPushButton::clicked,this,&SceneWindow::passMaterial);
    setWindowTitle(tr("Graphics Coursework 2"));
}

//Pass the materials to the GLWidget
void SceneWindow::passMaterial(){
    material->ambient[0] = ambient->text().toFloat();
    material->ambient[1] = ambient1->text().toFloat();
    material->ambient[2] = ambient2->text().toFloat();
    material->ambient[3] = ambient3->text().toFloat();
    material->diffuse[0] = diffuse->text().toFloat();
    material->diffuse[1] = diffuse1->text().toFloat();
    material->diffuse[2] = diffuse2->text().toFloat();
    material->diffuse[3] = diffuse3->text().toFloat();
    material->specular[0] = specular->text().toFloat();
    material->specular[1] = specular1->text().toFloat();
    material->specular[2] = specular2->text().toFloat();
    material->specular[3] = specular3->text().toFloat();
    material->shininess = shine->text().toFloat();
    this->sceneWidget->updateMaterial(material);
}

//Close all dialog windows
void SceneWindow::closeEvent(QCloseEvent *event){
    materialsDialog->close();
    worldDialog->close();
    rotationDialog->close();
}

