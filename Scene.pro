TEMPLATE = app
TARGET = Scene 
INCLUDEPATH += . /opt/local/include
CONFIG += c++11
QT += widgets opengl gui

LIBS += -lGLU -lglut

# Input
HEADERS += SceneWindow.h UnitsLabel.h SceneWidget.h Image.h Building.h Table.h
SOURCES += SceneMain.cpp \
           SceneWindow.cpp \
	   UnitsLabel.cpp \
	   SceneWidget.cpp \
	   Image.cpp

		                                                         
