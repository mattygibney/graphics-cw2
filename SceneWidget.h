//
// Created by pc on 17/12/2020.
//

#ifndef TEST_SCENEWIDGET_H
#define TEST_SCENEWIDGET_H
#include <QWidget>
#include <GL/gl.h>
#include <QtOpenGL/qgl.h>
#include "Image.h"

//Structure for material properties
typedef struct materialStruct {
    GLfloat ambient[4];
    GLfloat diffuse[4];
    GLfloat specular[4];
    GLfloat shininess;
} materialStruct;
static materialStruct windowMaterials = {
        {0.0,0.0,0.75,0.0},
        {0.0,0.0,0.75,0.0},
        {0.0,0.0,0.75,0.0},
        75.0};
static materialStruct whiteShinyMaterials = {
        { 1.0, 1.0, 1.0, 1.0},
        { 1.0, 1.0, 1.0, 1.0},
        { 1.0, 1.0, 1.0, 1.0},
        100.0
};

static materialStruct pearlMaterial = {
        {0.25,0.20725,0.20725},
        {1.,0.829,0.829},
        {0.296648,0.296648,0.296648},
        0.088

};

class SceneWidget:public QGLWidget{
    Q_OBJECT
    public:
    SceneWidget(QWidget *parent);

    public slots:
    void updateAngle();
    void changeAngle(int angle);
    void updateX(int x);
    void updateY(int y);
    void updateZ(int z);
    void paint();
    void cameraX(int x);
    void cameraY(int y);
    void cameraZ(int Z);
    void updateMaterial(struct materialStruct*);
    void toggleWall();
    protected:

    //OPENGL FUNCTIONS
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();



    //ROOM FUCNTIONS
    void garage(materialStruct *p_front);
    void cube(materialStruct *p_front);
    void square();
    void floor();
    void sphereCube();
    void building();
    void table();
    void tetrahedron();
    void cylinder();
    void world();



    private:
    int _sphereSlices=100;
    int _sphereStacks=100;
    float _angle;
    float _increment;
    float _speed;
    float _instances;
    float _changex=0.0;
    float _changey=0.0;
    float _startz=0.0;
    float _changez=0.0;
    float _camerax=0.0;
    float _cameray=0.0;
    float _cameraz=0.0;
    GLuint _texture_objects[4];
    Image _image0;
    Image _image1;
    Image _image2;
    Image _image3;
    GLUquadric* sphere;
    materialStruct *currentMaterial;
    GLfloat **frame_coords;
    int building_size;
    int block_size;

};


#endif //TEST_SCENEWIDGET_H
