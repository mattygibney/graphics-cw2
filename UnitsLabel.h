//
// Created by pc on 14/12/2020.
//
#ifndef TEST_UNITSLABEL_H
#define TEST_UNITSLABEL_H
#include <QLabel>


class UnitsLabel: public QLabel {
    Q_OBJECT
    public:
        UnitsLabel(QString units);
        ~UnitsLabel();
    public slots:
        void updateValue(int value);

    private:
    QString _units;
    QString _text;

};


#endif //TEST_UNITSLABEL_H
