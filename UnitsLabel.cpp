//
// Created by pc on 14/12/2020.
//

#include "UnitsLabel.h"
//Constructor
UnitsLabel::UnitsLabel(QString units){
    _units=units;
    this->setText(_units);
}
void UnitsLabel::updateValue(int value) {
    _text = QString("%1 ").arg(value) + _units;
    this->setText(_text);
}

UnitsLabel::~UnitsLabel() noexcept {}
